QT -= gui

CONFIG += c++11

TARGET = Nestor
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

INCLUDEPATH += ../lib/

LIBS += -L$$OUT_PWD/../lib -lnestor
