#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QTimer>

#include "nes.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QFile rom(a.arguments().at(1));
    Nes nes;

    QObject::connect(&nes, &Nes::initializationSucceeded, &nes, &Nes::run);
    QObject::connect(&nes, &Nes::errorOccured, [&a](const QString& message) {
        qCritical() << message;
        a.exit(1);
    });

    QTimer::singleShot(0, [&nes, &rom]() {
        nes.initialize(rom);
    });

    return a.exec();
}
