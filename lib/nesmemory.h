#ifndef NESMEMORY_H
#define NESMEMORY_H

#include <QObject>
#include <array>
#include <queue>

class Nes;

/**
 * The CPU accesses the following components through buses:
 * CPU RAM
 * I/O registers
 * Expension ROM
 * Save RAM (on the cartridge)
 * Program ROM (on the cartridge)
 *
 * r/w access is done through a memory map. Given an address,
 * a specific component will be accessed.
 *
 * This class abstracts these different hardware locations.
 */
class NesMemory : public QObject
{
    Q_OBJECT
public:
    explicit NesMemory(Nes* nes);

    /**
     * Reads a value from the memory
     * @param address the address to read at
     * @return the value which is at the given address
     */
    uint8_t read(uint16_t address);

    /**
     * Reads a value from the memory history
     * @return the oldest available history value
     */
    uint8_t readHistory();

    /**
     * Writes a value in the memory
     * @param address the address to write at
     * @param value the value to write
     */
    void write(uint16_t address, uint8_t value);

    uint8_t& operator[] (uint16_t address);

private:

    Nes* mNes;

    static constexpr uint16_t INTERNAL_MEMORY_SIZE = 0x8000;

    // All the NES addressable memory, except the cartridge
    std::array<uint8_t, INTERNAL_MEMORY_SIZE> mMemory;

    // Store last read bytes for automated test
    std::queue<uint8_t> mHistory;

    /**
     * Unmirror address
     *
     * Some internal memory ranges are mirrored several times. Instead of keeping
     * those areas synchronized, this method allows to convert an address that
     * belongs to a mirrored area back to the original area.
     *
     * If input address is not mirrored, return it.
     *
     * @param address input address to unmirror
     * @return unmirrored address
     */
    uint16_t unmirror(uint16_t address) const;
};

#endif // NESMEMORY_H
