#ifndef NES_H
#define NES_H

#include <QObject>

class QFile;

class Mos6502;
class NesPpu;
class NesMemory;
class NesCartridge;

class Nes : public QObject
{
    Q_OBJECT
public:
    explicit Nes(QObject *parent = 0);

    /**
     * initialize the NES emulator
     * @param romFile the file to load
     */
    void initialize(QFile& romFile);

    /**
     * run emulator
     */
    void run();

    Mos6502* cpu() const;

    NesMemory* memory() const;

    NesPpu* ppu() const;

    NesCartridge* cartridge() const;

private:
    // CPU
    Mos6502* mCpu;

    // Memory
    NesMemory* mMemory;

    // PPU
    NesPpu* mPpu;

    // Cartridge
    NesCartridge* mCartridge;

signals:
    void errorOccured(const QString& message);
    void initializationSucceeded();

};

#endif // NES_H
