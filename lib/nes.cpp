#include "nes.h"

#include <QTimer>

#include "mos6502.h"
#include "nesppu.h"
#include "nescartridge.h"
#include "nesmemory.h"

Nes::Nes(QObject *parent) : QObject(parent),
    mCpu(new Mos6502(this)),
    mMemory(new NesMemory(this)),
    mPpu(new NesPpu(this)),
    mCartridge(new NesCartridge(this))
{
    connect(mCartridge, &NesCartridge::errorOccured, this, &Nes::errorOccured);
    connect(mCartridge, &NesCartridge::fileLoaded, this, &Nes::initializationSucceeded);
}

void Nes::initialize(QFile& romFile)
{
    mCartridge->loadFromFile(romFile);
    // Read initial value for PC
    uint16_t programCounter = mMemory->read(0xFFFD) * 256;
    programCounter += mMemory->read(0xFFFC);
    mCpu->initialize(programCounter);
}

void Nes::run()
{
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, [this]() {
        mCpu->runCycle();
    });
    timer->start(100); // FIXME: slow cycles for debugging
}

Mos6502* Nes::cpu() const
{
    return mCpu;
}

NesPpu*Nes::ppu() const
{
    return mPpu;
}

NesMemory* Nes::memory() const
{
    return mMemory;
}

NesCartridge* Nes::cartridge() const
{
    return mCartridge;
}
