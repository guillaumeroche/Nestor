#include "nescartridge.h"

#include <QDataStream>
#include <QDebug>
#include <QFile>

#include "nes.h"

static constexpr uint8_t HEADER_SIZE = 16;  // bytes
static constexpr uint16_t PROGRAM_SIZE_MULTIPLIER = 16384;   // 16 KB
static constexpr uint16_t CHARACTER_SIZE_MULTIPLIER = 8192;  // 8 KB
static constexpr uint16_t TRAINER_SIZE = 512;

NesCartridge::NesCartridge(Nes* nes) : QObject(nes),
    mNes(nes),
    mMirroring(Mirroring::Unknown),
    mHasBatteryBackedRAM(false),
    mMapperType(0)
{

}

void NesCartridge::loadFromFile(QFile& romFile)
{
    if (!romFile.open(QIODevice::ReadOnly)) {
        emit errorOccured(tr("Unable to open file %1").arg(romFile.fileName()));
        return;
    }

    QDataStream inputStream(&romFile);

    // Read ROM header
    char header[HEADER_SIZE];
    if (inputStream.readRawData(header, HEADER_SIZE) != HEADER_SIZE) {
        emit errorOccured(tr("Invalid file"));
        return;
    }

    // Check header
    if (header[0] != 0x4E || header[1] != 0x45 || header[2] != 0x53 || header[3] != 0x1A) {
        emit errorOccured(tr("Invalid file header"));
        return;
    }

    // Mirroring
    mMirroring = static_cast<Mirroring>(header[6] & 0x01);

    // Persistent memory
    mHasBatteryBackedRAM = (header[6] & 0x02) == 0x02;

    // Mirroring override
    if (header[6] & 0x08) {
        mMirroring = Mirroring::FourScreen;
    }

    // Mapper lower nibble
    // Right shifting keeps the sign for signed types, so we need to cast to unsigned char
    mMapperType = ((uchar) header[6]) >> 4;

    // Mapper higher nibble
    mMapperType += header[7] & 0xF0;

    // Detect .nes file format version in 7th byte (http://wiki.nesdev.com/w/index.php/INES#Variant_comparison)
    NesFileVersion version = NesFileVersion::Archaic;
    if (header[7] && (header[7] & 0x0C) == 0) {
        qDebug() << "NES v1";
        version = NesFileVersion::Nes1;
    } else if (header[7] && (header[7] & 0x0C) == 0x08) {
        version = NesFileVersion::Nes2;
        qDebug() << "Nes v2";
        // TODO: also check size stuff
    }

    // Number of 8 KB RAM banks. For compatibility with previous
    // versions of the iNES format, assume 1 page of RAM when this is 0.
    mRAMBankNumber = header[8];
    if (mRAMBankNumber == 0) {
        mRAMBankNumber = 1;
    }

    // Read trainer if present
    if (header[6] && (header[6] & 0x04) == 0x04) {
        qDebug() << "This ROM contains a trainer";
        if (inputStream.readRawData(mTrainer.data(), TRAINER_SIZE) != TRAINER_SIZE) {
            emit errorOccured(tr("Could not read trainer data"));
            return;
        }
    }

    // Read program data
    const int32_t programSize = header[4] * PROGRAM_SIZE_MULTIPLIER;
    mProgramROM.resize(programSize);
    if (inputStream.readRawData(mProgramROM.data(), programSize) != programSize) {
        emit errorOccured(tr("Could not read program data"));
        return;
    }

    // Read character data
    const int32_t characterSize = header[5] * CHARACTER_SIZE_MULTIPLIER;
    mCharacterROM.resize(characterSize);
    if (inputStream.readRawData(mCharacterROM.data(), characterSize) != characterSize) {
        emit errorOccured(tr("Could not read character data"));
        return;
    }

    emit fileLoaded();
}

uint8_t NesCartridge::read(uint16_t address) const
{
    if (address < mProgramROM.size()) {
        return static_cast<uint8_t>(mProgramROM[address]);
    }

    // Program rom is mirrored if < 16k
    if ((mProgramROM.size() <= PROGRAM_SIZE_MULTIPLIER) && (address >= PROGRAM_SIZE_MULTIPLIER)) {
        address -= PROGRAM_SIZE_MULTIPLIER;
        if (address < mProgramROM.size()) {
            return static_cast<uint8_t>(mProgramROM[address]);
        }
    }

    qWarning() << "Address is too large:" << address;
    return 0;
}
