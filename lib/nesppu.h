#ifndef NESPPU_H
#define NESPPU_H

#include <QObject>
#include <array>

class Nes;

class NesPpu : public QObject
{
    Q_OBJECT
public:
    explicit NesPpu(Nes* nes);

    /**
     * Runs a single PPU cycle.
     * A Scanline lasts for 341 cycles.
     * There are 262 scanlines to render per frame.
     */
    void runCycle();

    /**
     * Reads a value from the memory
     * @param address the address to read at
     * @return the value which is at the given address
     */
    uint8_t read(uint16_t address) const;

    /**
     * Writes a value in the memory
     * @param address the address to write at
     * @param value the value to write
     */
    void write(uint16_t address, uint8_t value);

    // Getters for registers
    // Used for testing
    uint8_t& controlRegister1() const;
    uint8_t& controlRegister2() const;
    uint8_t& statusRegister() const;
    uint8_t& spriteAddressRegister() const;
    uint8_t& spriteIORegister() const;
    uint8_t& videoAddressRegister1() const;
    uint8_t& videoAddressRegister2() const;
    uint8_t& videoIORegister() const;
    uint8_t& spriteDMARegister() const;

private:
    static constexpr uint16_t VIDEO_MEMORY_SIZE = 0x4000;
    static constexpr uint16_t SPRITE_MEMORY_SIZE = 256;

    std::array<uint8_t, VIDEO_MEMORY_SIZE> mVideoMemory;    // VRAM
    std::array<uint8_t, SPRITE_MEMORY_SIZE> mSpriteMemory;  // SPR-RAM

    uint16_t mScanline = 0;  // Vary between 0 to 261
    uint16_t mCycle = 0;     // Vary between 0 to 340

    // Remaining cycles counter
    uint8_t mRemainingCycles = 0;

    // Registers

    // They are memory-mapped on addresses 0x2000 - 0x2007.
    // R: the register is read-only for emulated code. It is set by the PPU.
    // W: the register is written by emulated code to configure the PPU.

    enum ControlRegister1Bit {
        NameTableAddress1 = 0x01,
        NameTableAddress2 = 0x02,
        AddressIncrement = 0x04,
        SpritePatternTable = 0x08,
        BackgroundPatternTable = 0x10,
        SpriteSize = 0x20,
        // 6th bit unused on the NES
        NMIOnVBlank = 0x80,
    };

    // PPU control register 1 (W)
    uint8_t& mControlRegister1;

    enum ControlRegister2Bit {
        Monochrome = 0x01,
        BackgroundClipping = 0x02,
        SpriteClipping = 0x04,
        BackgroundDisplay = 0x08,
        SpriteDisplay = 0x10,
        Color1 = 0x20,
        Color2 = 0x40,
        Color3 = 0x80,
    };

    // PPU control register 2 (W)
    uint8_t& mControlRegister2;

    enum StatusFlag {
        // First bits are unused
        IgnoreVRAMWrite = 0x10,
        ScanlineSpriteCount = 0x20,
        Sprite0Hit = 0x40,
        VBlank = 0x80,
    };

    // PPU status register (R)
    uint8_t& mStatusRegister;

    // SPR-RAM address register (W)
    uint8_t& mSpriteAddressRegister;

    // SPR-RAM I/O register (W)
    uint8_t& mSpriteIORegister;

    // VRAM address register 1 (W)
    uint8_t& mVideoAddressRegister1;

    // VRAM address register 2 (W)
    uint8_t& mVideoAddressRegister2;

    // VRAM I/O register (R/W)
    uint8_t& mVideoIORegister;

    // Sprite DMA register (W)
    uint8_t& mSpriteDMARegister;

    /**
     * Unmirror address
     *
     * Some internal memory ranges are mirrored several times. Instead of keeping
     * those areas synchronized, this method allows to convert an address that
     * belongs to a mirrored area back to the original area.
     *
     * If input address is not mirrored, return it.
     *
     * @param address input address to unmirror
     * @return unmirrored address
     */
    uint16_t unmirror(uint16_t address) const;
};

#endif // NESPPU_H
