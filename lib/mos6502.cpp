#include "mos6502.h"

#include "nes.h"
#include "nesmemory.h"

#define CALL_OPCODE_FN(ptrToMember)  (this->*ptrToMember)()

constexpr uint8_t opcodeCycles[] = {
    7, 6, 2, 8, 3, 3, 5, 5, 3, 2, 2, 2, 4, 4, 6, 6,
    2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    6, 6, 2, 8, 3, 3, 5, 5, 4, 2, 2, 2, 4, 4, 6, 6,
    2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    6, 6, 2, 8, 3, 3, 5, 5, 3, 2, 2, 2, 3, 4, 6, 6,
    2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    6, 6, 2, 8, 3, 3, 5, 5, 4, 2, 2, 2, 5, 4, 6, 6,
    2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    2, 6, 2, 6, 3, 3, 3, 3, 2, 2, 2, 2, 4, 4, 4, 4,
    2, 6, 2, 6, 4, 4, 4, 4, 2, 5, 2, 5, 5, 5, 5, 5,
    2, 6, 2, 6, 3, 3, 3, 3, 2, 2, 2, 2, 4, 4, 4, 4,
    2, 5, 2, 5, 4, 4, 4, 4, 2, 4, 2, 4, 4, 4, 4, 4,
    2, 6, 2, 8, 3, 3, 5, 5, 2, 2, 2, 2, 4, 4, 6, 6,
    2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    2, 6, 3, 8, 3, 3, 5, 5, 2, 2, 2, 2, 4, 4, 6, 6,
    2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
};

constexpr uint8_t opcodeBoundaryCycles[] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
};

constexpr Mos6502::Addressing opcodeAddressingModes[] {
    Mos6502::Imp, Mos6502::InX, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::ZPg, Mos6502::ZPg, Mos6502::Non,
    Mos6502::Imp, Mos6502::Imm, Mos6502::Acc, Mos6502::Non, Mos6502::Non, Mos6502::Abs, Mos6502::Abs, Mos6502::Non,
    Mos6502::Rel, Mos6502::InY, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::ZPX, Mos6502::ZPX, Mos6502::Non,
    Mos6502::Imp, Mos6502::AbY, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::AbX, Mos6502::AbX, Mos6502::Non,
    Mos6502::Abs, Mos6502::InX, Mos6502::Non, Mos6502::Non, Mos6502::ZPg, Mos6502::ZPg, Mos6502::ZPg, Mos6502::Non,
    Mos6502::Imp, Mos6502::Imm, Mos6502::Acc, Mos6502::Non, Mos6502::Abs, Mos6502::Abs, Mos6502::Abs, Mos6502::Non,
    Mos6502::Rel, Mos6502::InY, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::ZPX, Mos6502::ZPX, Mos6502::Non,
    Mos6502::Imp, Mos6502::AbY, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::AbX, Mos6502::AbX, Mos6502::Non,
    Mos6502::Imp, Mos6502::InX, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::ZPg, Mos6502::ZPg, Mos6502::Non,
    Mos6502::Imp, Mos6502::Imm, Mos6502::Acc, Mos6502::Non, Mos6502::Abs, Mos6502::Abs, Mos6502::Abs, Mos6502::Non,
    Mos6502::Rel, Mos6502::InY, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::ZPX, Mos6502::ZPX, Mos6502::Non,
    Mos6502::Imp, Mos6502::AbY, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::AbX, Mos6502::AbX, Mos6502::Non,
    Mos6502::Imp, Mos6502::InX, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::ZPg, Mos6502::ZPg, Mos6502::Non,
    Mos6502::Imp, Mos6502::Imm, Mos6502::Acc, Mos6502::Non, Mos6502::Ind, Mos6502::Abs, Mos6502::Abs, Mos6502::Non,
    Mos6502::Rel, Mos6502::InY, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::ZPX, Mos6502::ZPX, Mos6502::Non,
    Mos6502::Imp, Mos6502::AbY, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::AbX, Mos6502::AbX, Mos6502::Non,
    Mos6502::Non, Mos6502::InX, Mos6502::Non, Mos6502::Non, Mos6502::ZPg, Mos6502::ZPg, Mos6502::ZPg, Mos6502::Non,
    Mos6502::Imp, Mos6502::Non, Mos6502::Imp, Mos6502::Non, Mos6502::Abs, Mos6502::Abs, Mos6502::Abs, Mos6502::Non,
    Mos6502::Rel, Mos6502::InY, Mos6502::Non, Mos6502::Non, Mos6502::ZPX, Mos6502::ZPX, Mos6502::ZPY, Mos6502::Non,
    Mos6502::Imp, Mos6502::AbY, Mos6502::Imp, Mos6502::Non, Mos6502::Non, Mos6502::AbX, Mos6502::Non, Mos6502::Non,
    Mos6502::Imm, Mos6502::InX, Mos6502::Imm, Mos6502::Non, Mos6502::ZPg, Mos6502::ZPg, Mos6502::ZPg, Mos6502::Non,
    Mos6502::Imp, Mos6502::Imm, Mos6502::Imp, Mos6502::Non, Mos6502::Abs, Mos6502::Abs, Mos6502::Abs, Mos6502::Non,
    Mos6502::Rel, Mos6502::InY, Mos6502::Non, Mos6502::Non, Mos6502::ZPX, Mos6502::ZPX, Mos6502::ZPY, Mos6502::Non,
    Mos6502::Imp, Mos6502::AbY, Mos6502::Imp, Mos6502::Non, Mos6502::AbX, Mos6502::AbX, Mos6502::AbY, Mos6502::Non,
    Mos6502::Imm, Mos6502::InX, Mos6502::Non, Mos6502::Non, Mos6502::ZPg, Mos6502::ZPg, Mos6502::ZPg, Mos6502::Non,
    Mos6502::Imp, Mos6502::Imm, Mos6502::Imp, Mos6502::Non, Mos6502::Abs, Mos6502::Abs, Mos6502::Abs, Mos6502::Non,
    Mos6502::Rel, Mos6502::InY, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::ZPX, Mos6502::ZPX, Mos6502::Non,
    Mos6502::Imp, Mos6502::AbY, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::AbX, Mos6502::AbX, Mos6502::Non,
    Mos6502::Imm, Mos6502::InX, Mos6502::Non, Mos6502::Non, Mos6502::ZPg, Mos6502::ZPg, Mos6502::ZPg, Mos6502::Non,
    Mos6502::Imp, Mos6502::Imm, Mos6502::Imp, Mos6502::Non, Mos6502::Abs, Mos6502::Abs, Mos6502::Abs, Mos6502::Non,
    Mos6502::Rel, Mos6502::InY, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::ZPX, Mos6502::ZPX, Mos6502::Non,
    Mos6502::Imp, Mos6502::AbY, Mos6502::Non, Mos6502::Non, Mos6502::Non, Mos6502::AbX, Mos6502::AbX, Mos6502::Non,
};

Mos6502::Mos6502(Nes* nes) : QObject(nes),
    mStackPointer(0xFD),
    mStatusRegister(0),
    mAccumulator(0),
    mXRegister(0),
    mYRegister(0),
    mPageBoundaryCrossed(false),
    mRemainingCycles(0),
    mNes(nes)
{
    // Bit 5 is unused, but always set
    setFlag(Bit5, true);
    setFlag(InterruptDisable, true);
}

void Mos6502::initialize(uint16_t programCounter)
{
    mProgramCounter = programCounter;
}

void Mos6502::runCycle()
{
    static OpcodeFunction opcodesFunctions[] = {
        &Mos6502::BRK, &Mos6502::ORA, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::ORA, &Mos6502::ASL, &Mos6502::UND,
        &Mos6502::PHP, &Mos6502::ORA, &Mos6502::ASL, &Mos6502::UND, &Mos6502::UND, &Mos6502::ORA, &Mos6502::ASL, &Mos6502::UND,
        &Mos6502::BPL, &Mos6502::ORA, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::ORA, &Mos6502::ASL, &Mos6502::UND,
        &Mos6502::CLC, &Mos6502::ORA, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::ORA, &Mos6502::ASL, &Mos6502::UND,
        &Mos6502::JSR, &Mos6502::AND, &Mos6502::UND, &Mos6502::UND, &Mos6502::BIT, &Mos6502::AND, &Mos6502::ROL, &Mos6502::UND,
        &Mos6502::PLP, &Mos6502::AND, &Mos6502::ROL, &Mos6502::UND, &Mos6502::BIT, &Mos6502::AND, &Mos6502::ROL, &Mos6502::UND,
        &Mos6502::BMI, &Mos6502::AND, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::AND, &Mos6502::ROL, &Mos6502::UND,
        &Mos6502::SEC, &Mos6502::AND, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::AND, &Mos6502::ROL, &Mos6502::UND,
        &Mos6502::RTI, &Mos6502::EOR, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::EOR, &Mos6502::LSR, &Mos6502::UND,
        &Mos6502::PHA, &Mos6502::EOR, &Mos6502::LSR, &Mos6502::UND, &Mos6502::JMP, &Mos6502::EOR, &Mos6502::LSR, &Mos6502::UND,
        &Mos6502::BVC, &Mos6502::EOR, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::EOR, &Mos6502::LSR, &Mos6502::UND,
        &Mos6502::CLI, &Mos6502::EOR, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::EOR, &Mos6502::LSR, &Mos6502::UND,
        &Mos6502::RTS, &Mos6502::ADC, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::ADC, &Mos6502::ROR, &Mos6502::UND,
        &Mos6502::PLA, &Mos6502::ADC, &Mos6502::ROR, &Mos6502::UND, &Mos6502::JMP, &Mos6502::ADC, &Mos6502::ROR, &Mos6502::UND,
        &Mos6502::BVS, &Mos6502::ADC, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::ADC, &Mos6502::ROR, &Mos6502::UND,
        &Mos6502::SEI, &Mos6502::ADC, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::ADC, &Mos6502::ROR, &Mos6502::UND,
        &Mos6502::UND, &Mos6502::STA, &Mos6502::UND, &Mos6502::UND, &Mos6502::STY, &Mos6502::STA, &Mos6502::STX, &Mos6502::UND,
        &Mos6502::DEY, &Mos6502::UND, &Mos6502::TXA, &Mos6502::UND, &Mos6502::STY, &Mos6502::STA, &Mos6502::STX, &Mos6502::UND,
        &Mos6502::BCC, &Mos6502::STA, &Mos6502::UND, &Mos6502::UND, &Mos6502::STY, &Mos6502::STA, &Mos6502::STX, &Mos6502::UND,
        &Mos6502::TYA, &Mos6502::STA, &Mos6502::TXS, &Mos6502::UND, &Mos6502::UND, &Mos6502::STA, &Mos6502::UND, &Mos6502::UND,
        &Mos6502::LDY, &Mos6502::LDA, &Mos6502::LDX, &Mos6502::UND, &Mos6502::LDY, &Mos6502::LDA, &Mos6502::LDX, &Mos6502::UND,
        &Mos6502::TAY, &Mos6502::LDA, &Mos6502::TAX, &Mos6502::UND, &Mos6502::LDY, &Mos6502::LDA, &Mos6502::LDX, &Mos6502::UND,
        &Mos6502::BCS, &Mos6502::LDA, &Mos6502::UND, &Mos6502::UND, &Mos6502::LDY, &Mos6502::LDA, &Mos6502::LDX, &Mos6502::UND,
        &Mos6502::CLV, &Mos6502::LDA, &Mos6502::TSX, &Mos6502::UND, &Mos6502::LDY, &Mos6502::LDA, &Mos6502::LDX, &Mos6502::UND,
        &Mos6502::CPY, &Mos6502::CMP, &Mos6502::UND, &Mos6502::UND, &Mos6502::CPY, &Mos6502::CMP, &Mos6502::DEC, &Mos6502::UND,
        &Mos6502::INY, &Mos6502::CMP, &Mos6502::DEX, &Mos6502::UND, &Mos6502::CPY, &Mos6502::CMP, &Mos6502::DEC, &Mos6502::UND,
        &Mos6502::BNE, &Mos6502::CMP, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::CMP, &Mos6502::DEC, &Mos6502::UND,
        &Mos6502::CLD, &Mos6502::CMP, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::CMP, &Mos6502::DEC, &Mos6502::UND,
        &Mos6502::CPX, &Mos6502::SBC, &Mos6502::UND, &Mos6502::UND, &Mos6502::CPX, &Mos6502::SBC, &Mos6502::INC, &Mos6502::UND,
        &Mos6502::INX, &Mos6502::SBC, &Mos6502::NOP, &Mos6502::UND, &Mos6502::CPX, &Mos6502::SBC, &Mos6502::INC, &Mos6502::UND,
        &Mos6502::BEQ, &Mos6502::SBC, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::SBC, &Mos6502::INC, &Mos6502::UND,
        &Mos6502::SED, &Mos6502::SBC, &Mos6502::UND, &Mos6502::UND, &Mos6502::UND, &Mos6502::SBC, &Mos6502::INC, &Mos6502::UND,
    };

    if (mRemainingCycles == 0) {
        mPageBoundaryCrossed = false;
        uint8_t opcode = mNes->memory()->read(mProgramCounter++);
        uint16_t initialPage = mProgramCounter & 0xFF00;

        mAddressingMode = opcodeAddressingModes[opcode];

        CALL_OPCODE_FN(opcodesFunctions[opcode]);

        mRemainingCycles += opcodeCycles[opcode];
        if (mPageBoundaryCrossed || initialPage != (mProgramCounter & 0xFF00)) {
            mRemainingCycles += opcodeBoundaryCycles[opcode];
        }
    }

    if (mRemainingCycles > 0) {
        mRemainingCycles--;
    }

}

bool Mos6502::isFlagEnabled(Mos6502::StatusFlag flag)
{
    return mStatusRegister & flag;
}

void Mos6502::setFlag(Mos6502::StatusFlag flag, bool enabled)
{
    if (enabled) {
        mStatusRegister |= flag;
    } else {
        mStatusRegister &= ~flag;
    }
}

void Mos6502::setZNFlags(uint8_t value)
{
    setFlag(StatusFlag::Zero, value == 0);
    setFlag(StatusFlag::Negative, value & 0x80);
}

uint16_t Mos6502::readNextAddress()
{
    // least significant byte comes first
    uint16_t address = mNes->memory()->read(mProgramCounter++);
    // then most significant byte
    address += mNes->memory()->read(mProgramCounter++) << 8;
    return address;
}

uint16_t Mos6502::readAddressAt(uint16_t address) const
{
    // least significant byte comes first
    uint16_t targetAddress = mNes->memory()->read(address);
    // then most significant byte
    targetAddress += mNes->memory()->read(address + 1) << 8;
    return targetAddress;
}

uint16_t Mos6502::readAddressFromZeroPage(uint8_t address)
{
    if (address == 0xff) {
        mPageBoundaryCrossed = true;
    }
    // least significant byte comes first
    uint16_t targetAddress = mNes->memory()->read(address);
    // then most significant byte
    targetAddress += mNes->memory()->read(static_cast<uint8_t>(address + 1)) << 8;
    return targetAddress;
}

uint16_t Mos6502::resolveAddress()
{
    uint16_t address = 0;
    switch (mAddressingMode) {
    case Addressing::ZPg:
        address = mNes->memory()->read(mProgramCounter++);
        break;
    case Addressing::ZPX:
        address = mNes->memory()->read(mProgramCounter++) + mXRegister;
        // Wraparound: address is in zero page [0x0000; 0x00FF], so only return least significant byte
        address &= 0x00FF;
        break;
    case Addressing::ZPY:
        address = mNes->memory()->read(mProgramCounter++) + mYRegister;
        // Wraparound: address is in zero page [0x0000; 0x00FF], so only return least significant byte
        address &= 0x00FF;
        break;
    case Addressing::Abs:
        address = readNextAddress();
        break;
    case Addressing::AbX:
        address = readNextAddress();
        if ((address & 0xFF) + mXRegister > 0xFF) {
            mPageBoundaryCrossed = true;
        }
        address += mXRegister;
        break;
    case Addressing::AbY:
        address = readNextAddress();
        if ((address & 0xFF) + mYRegister > 0xFF) {
            mPageBoundaryCrossed = true;
        }
        address += mYRegister;
        break;
    case Addressing::Ind: {
        // This mode is only used by the JMP instruction, and bugged when the
        // LSB is at an 0xXXFF address. Hence the specific implementation below
        // See: http://obelisk.me.uk/6502/reference.html#JMP
        uint16_t nextAddress = readNextAddress();

        // least significant byte comes first
        address = mNes->memory()->read(nextAddress);
        // then most significant byte
        if ((nextAddress & 0xFF) == 0xFF) {
            address += mNes->memory()->read(nextAddress & 0xFF00) << 8;
        } else {
            address += mNes->memory()->read(nextAddress + 1) << 8;
        }
        break;
    }
    case Addressing::Imm:
        address = mProgramCounter;
        mProgramCounter++;
        break;
    case Addressing::InX:
    {
        uint8_t indirectAddress = mNes->memory()->read(mProgramCounter++) + mXRegister;
        address = readAddressFromZeroPage(indirectAddress);
        break;
    }
    case Addressing::InY:
        address = readAddressFromZeroPage(mNes->memory()->read(mProgramCounter++));
        if ((address & 0xFF) + mYRegister > 0xFF) {
            mPageBoundaryCrossed = true;
        }
        address += mYRegister;
        break;
    default:
        break;
    }
    return address;
}

uint8_t Mos6502::readAddressedValue()
{
    return readAddressedValue(resolveAddress());
}

void Mos6502::writeAddressedValue(uint8_t value)
{
    writeAddressedValue(resolveAddress(), value);
}

uint8_t Mos6502::readAddressedValue(uint16_t address)
{
    if (mAddressingMode == Addressing::Acc) {
        return mAccumulator;
    }
    return mNes->memory()->read(address);
}

void Mos6502::writeAddressedValue(uint16_t address, uint8_t value)
{
    if (mAddressingMode == Addressing::Acc) {
        mAccumulator = value;
    } else {
        mNes->memory()->write(address, value);
    }
}

void Mos6502::pushStack(uint8_t value)
{
    (*mNes->memory())[0x0100 + mStackPointer] = value;
    mStackPointer--;
}

uint8_t Mos6502::pullStack()
{
    mStackPointer++;
    return mNes->memory()->read(0x0100 + mStackPointer);
}

void Mos6502::ADC()
{
    uint8_t operand1 = mAccumulator;
    uint8_t operand2 = readAddressedValue();
    uint32_t result = operand1 + operand2 + (mStatusRegister & StatusFlag::Carry);
    mAccumulator = static_cast<uint8_t>(result);

    setFlag(StatusFlag::Carry, result > 0xFF);
    // See http://www.righto.com/2012/12/the-6502-overflow-flag-explained.html
    setFlag(StatusFlag::Overflow, ((operand1 ^ mAccumulator) & (operand2 ^ mAccumulator) & 0x80) != 0);
    setZNFlags(mAccumulator);
}


void Mos6502::AND()
{
    mAccumulator &= readAddressedValue();
    setZNFlags(mAccumulator);
}


void Mos6502::ASL()
{
    uint16_t address = resolveAddress();
    uint8_t value = readAddressedValue(address);
    // Original bit 7 goes to carry
    setFlag(StatusFlag::Carry, value & 0x80);
    value <<= 1;
    writeAddressedValue(address, value);
    setZNFlags(value);
}


void Mos6502::BCC()
{
    uint8_t operand = mNes->memory()->read(mProgramCounter++);
    if (!(mStatusRegister & StatusFlag::Carry)) {
        mProgramCounter += operand;
        mRemainingCycles++;
    }
}


void Mos6502::BCS()
{
    uint8_t operand = mNes->memory()->read(mProgramCounter++);
    if (mStatusRegister & StatusFlag::Carry) {
        mProgramCounter += operand;
        mRemainingCycles++;
    }
}


void Mos6502::BEQ()
{
    uint8_t operand = mNes->memory()->read(mProgramCounter++);
    if (mStatusRegister & StatusFlag::Zero) {
        mProgramCounter += operand;
        mRemainingCycles++;
    }
}


void Mos6502::BIT()
{
    uint8_t value = readAddressedValue();
    uint8_t result = mAccumulator & value;
    setFlag(StatusFlag::Zero, result == 0);
    // 6th bit of value is copied to overflow flag
    setFlag(StatusFlag::Overflow, (1 << 6) & value);
    // 7th bit of value is copied to negative flag
    setFlag(StatusFlag::Negative, (1 << 7) & value);
}


void Mos6502::BMI()
{
    uint8_t operand = mNes->memory()->read(mProgramCounter++);
    if (mStatusRegister & StatusFlag::Negative) {
        mProgramCounter += operand;
        mRemainingCycles++;
    }
}


void Mos6502::BNE()
{
    uint8_t operand = mNes->memory()->read(mProgramCounter++);
    if (!(mStatusRegister & StatusFlag::Zero)) {
        mProgramCounter += operand;
        mRemainingCycles++;
    }
}


void Mos6502::BPL()
{
    uint8_t operand = mNes->memory()->read(mProgramCounter++);
    if (!(mStatusRegister & StatusFlag::Negative)) {
        mProgramCounter += operand;
        mRemainingCycles++;
    }
}


void Mos6502::BRK()
{
    setFlag(StatusFlag::BreakCommand, true);
    PHP();
    pushStack(mProgramCounter >> 8);
    pushStack(mProgramCounter & 0x00FF);
    mProgramCounter = 0xFFFE;
}


void Mos6502::BVC()
{
    uint8_t operand = mNes->memory()->read(mProgramCounter++);
    if (!(mStatusRegister & StatusFlag::Overflow)) {
        mProgramCounter += operand;
        mRemainingCycles++;
    }
}


void Mos6502::BVS()
{
    uint8_t operand = mNes->memory()->read(mProgramCounter++);
    if (mStatusRegister & StatusFlag::Overflow) {
        mProgramCounter += operand;
        mRemainingCycles++;
    }
}


void Mos6502::CLC()
{
    setFlag(StatusFlag::Carry, false);
}


void Mos6502::CLD()
{
    setFlag(StatusFlag::DecimalMode, false);
}


void Mos6502::CLI()
{
    setFlag(StatusFlag::InterruptDisable, false);
}


void Mos6502::CLV()
{
    setFlag(StatusFlag::Overflow, false);
}

void Mos6502::compare(uint8_t value1, uint8_t value2)
{
    setZNFlags(value1 - value2);
    setFlag(StatusFlag::Carry, value1 >= value2);
}


void Mos6502::CMP()
{
    compare(mAccumulator, readAddressedValue());
}


void Mos6502::CPX()
{
    compare(mXRegister, readAddressedValue());
}


void Mos6502::CPY()
{
    compare(mYRegister, readAddressedValue());
}


void Mos6502::DEC()
{
    const uint16_t address = resolveAddress();
    (*mNes->memory())[address]--;
    setZNFlags((*mNes->memory())[address]);
}


void Mos6502::DEX()
{
    mXRegister--;
    setZNFlags(mXRegister);
}


void Mos6502::DEY()
{
    mYRegister--;
    setZNFlags(mYRegister);
}


void Mos6502::EOR()
{
    mAccumulator ^= readAddressedValue();
    setZNFlags(mAccumulator);
}


void Mos6502::INC()
{
    const uint16_t address = resolveAddress();
    (*mNes->memory())[address]++;
    setZNFlags((*mNes->memory())[address]);
}


void Mos6502::INX()
{
    mXRegister++;
    setZNFlags(mXRegister);
}


void Mos6502::INY()
{
    mYRegister++;
    setZNFlags(mYRegister);
}


void Mos6502::JMP()
{
    mProgramCounter = resolveAddress();
}


void Mos6502::JSR()
{
    uint16_t address = resolveAddress();
    // Store PC - 1 on the Stack, see http://idoc64.free.fr/ASM/base.htm#JSR
    mProgramCounter--;
    pushStack(mProgramCounter >> 8);
    pushStack(mProgramCounter & 0x00FF);
    mProgramCounter = address;
}


void Mos6502::LDA()
{
    mAccumulator = readAddressedValue();
    setZNFlags(mAccumulator);
}


void Mos6502::LDX()
{
    mXRegister = readAddressedValue();
    setZNFlags(mXRegister);
}


void Mos6502::LDY()
{
    mYRegister = readAddressedValue();
    setZNFlags(mYRegister);
}


void Mos6502::LSR()
{
    uint16_t address = resolveAddress();
    uint8_t value = readAddressedValue(address);
    // Original bit 0 goes to carry
    setFlag(StatusFlag::Carry, value & 0x01);
    value >>= 1;
    writeAddressedValue(address, value);
    setZNFlags(value);
}


void Mos6502::NOP()
{
}


void Mos6502::ORA()
{
    mAccumulator |= readAddressedValue();
    setZNFlags(mAccumulator);
}


void Mos6502::PHA()
{
    pushStack(mAccumulator);
}


void Mos6502::PHP()
{
    // Bit 4 is always set to 1 when pushing P on the stack from PHP or BRK instructions.
    // See http://wiki.nesdev.com/w/index.php/Status_flags
    pushStack(mStatusRegister | StatusFlag::BreakCommand);
}


void Mos6502::PLA()
{
    mAccumulator = pullStack();
    setZNFlags(mAccumulator);
}


void Mos6502::PLP()
{
    mStatusRegister = pullStack();
    // Bit 4 is never stored as 1 in the register: http://wiki.nesdev.com/w/index.php/CPU_status_flag_behavior
    setFlag(BreakCommand, false);
    // Bit 5 is always stored as 1 in the register
    setFlag(Bit5, true);
}


void Mos6502::ROL()
{
    uint16_t address = resolveAddress();
    uint8_t value = readAddressedValue(address);
    bool carry = isFlagEnabled(StatusFlag::Carry);
    // Original bit 7 goes to carry
    setFlag(StatusFlag::Carry, value & 0x80);
    value <<= 1;
    // Original carry goes to bit 0
    if (carry) {
        value |= 0x01;
    }
    writeAddressedValue(address, value);
    setZNFlags(value);
}


void Mos6502::ROR()
{
    uint16_t address = resolveAddress();
    uint8_t value = readAddressedValue(address);
    bool carry = isFlagEnabled(StatusFlag::Carry);
    // Original bit 0 goes to carry
    setFlag(StatusFlag::Carry, value & 0x01);
    value >>= 1;
    // Original carry goes to bit 7
    if (carry) {
        value |= 0x80;
    }
    writeAddressedValue(address, value);
    setZNFlags(value);
}


void Mos6502::RTI()
{
    mStatusRegister = pullStack();
    // Bit 5 is always stored as 1 in the register
    setFlag(Bit5, true);
    mProgramCounter = pullStack();
    mProgramCounter += pullStack() << 8;
}


void Mos6502::RTS()
{
    mProgramCounter = pullStack();
    mProgramCounter += pullStack() << 8;
    mProgramCounter++;
}


void Mos6502::SBC()
{
    // See http://www.righto.com/2012/12/the-6502-overflow-flag-explained.html
    uint8_t operand1 = mAccumulator;
    uint8_t operand2 = readAddressedValue();
    uint32_t result = operand1 + 255 - operand2 + (mStatusRegister & StatusFlag::Carry);
    mAccumulator = static_cast<uint8_t>(result);

    setFlag(StatusFlag::Carry, result > 0xFF);
    setFlag(StatusFlag::Overflow, ((operand1 ^ mAccumulator) & ((255 - operand2) ^ mAccumulator) & 0x80) != 0);
    setZNFlags(mAccumulator);
}


void Mos6502::SEC()
{
    setFlag(StatusFlag::Carry, true);
}


void Mos6502::SED()
{
    setFlag(StatusFlag::DecimalMode, true);
}


void Mos6502::SEI()
{
    setFlag(StatusFlag::InterruptDisable, true);
}


void Mos6502::STA()
{
    writeAddressedValue(mAccumulator);
}


void Mos6502::STX()
{
    writeAddressedValue(mXRegister);
}


void Mos6502::STY()
{
    writeAddressedValue(mYRegister);
}


void Mos6502::TAX()
{
    mXRegister = mAccumulator;
    setZNFlags(mXRegister);
}


void Mos6502::TAY()
{
    mYRegister = mAccumulator;
    setZNFlags(mYRegister);
}


void Mos6502::TSX()
{
    mXRegister = mStackPointer;
    setZNFlags(mXRegister);
}


void Mos6502::TXA()
{
    mAccumulator = mXRegister;
    setZNFlags(mAccumulator);
}


void Mos6502::TXS()
{
    mStackPointer = mXRegister;
}


void Mos6502::TYA()
{
    mAccumulator = mYRegister;
    setZNFlags(mAccumulator);
}


void Mos6502::UND()
{
}
