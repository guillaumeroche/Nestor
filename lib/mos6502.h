#ifndef MOS6502_H
#define MOS6502_H

#include <QObject>

class Nes;

/**
 * The Mos6502 class emulates the MOS 6502 8-bit processor
 */
class Mos6502 : public QObject
{
    Q_OBJECT

    friend class tst_mos6502;

public:
    explicit Mos6502(Nes* nes);

    // Flag layout in the Status Register: [N, V, /, B, D, I, Z, C]
    // Bit 5 is unused.
    enum StatusFlag {
        Carry = 0x01,
        Zero = 0x02,
        InterruptDisable = 0x04,
        DecimalMode = 0x08,
        BreakCommand = 0x10,
        Bit5 = 0x20,
        Overflow = 0x40,
        Negative = 0x80
    };
    Q_DECLARE_FLAGS(Status, StatusFlag)

    enum Addressing {
        Non,
        ZPg,
        ZPX,
        ZPY,
        Abs,
        AbX,
        AbY,
        Ind,
        Imp,
        Acc,
        Imm,
        Rel,
        InX,
        InY
    };

    void initialize(uint16_t programCounter);

    /**
     * Runs a single CPU cycle
     */
    void runCycle();

private:

    /**
     * Checks if a given flag is enabled
     * @param flag the flag to check
     * @return true if the flag is enabled
     */
    bool isFlagEnabled(StatusFlag flag);

    /**
     * Enables or disables a given flag
     * @param flag the flag to set
     * @param enabled true to enable the flag
     */
    void setFlag(StatusFlag flag, bool enabled);

    /**
     * Sets the Z and N flags, given a value
     * @param value
     */
    void setZNFlags(uint8_t value);

    /**
     * Reads the 2 bytes address that comes next in the program counter
     *
     * @return a 2 bytes address
     */
    uint16_t readNextAddress();

    /**
     * Reads a 2 bytes address from specified address
     * @param address the address of the address
     * @return the required address
     */
    uint16_t readAddressAt(uint16_t address) const;

    /**
     * Reads a 2 bytes address from specified address, from the zero page memory [0x00; 0xFF]
     * @param address the address of the address
     * @return the required address
     */
    uint16_t readAddressFromZeroPage(uint8_t address);

    /**
     * Resolves current instruction address according to the addressing mode
     * It relies on program counter and / or registers to compute the address
     *
     * @return the resolved address
     */
    uint16_t resolveAddress();

    /**
     * Reads a value from memory, depending on current addressing mode
     * @return the value
     */
    uint8_t readAddressedValue();

    /**
     * Writes a value in memory, depending on current addressing mode
     * @param value the value to write
     */
    void writeAddressedValue(uint8_t value);

    /**
     * Reads a value from memory, depending on current addressing mode
     * @param address the address to read from
     * @return the value
     */
    uint8_t readAddressedValue(uint16_t address);

    /**
     * Writes a value in memory, depending on current addressing mode
     * @param address the address to write to
     * @param value the value to write
     */
    void writeAddressedValue(uint16_t address, uint8_t value);

    /**
     * Puts a value on the stack, and updates the stack pointer
     * @param value the value to push on the stack
     */
    void pushStack(uint8_t value);

    /**
     * Removes a value from the stack, and updates the stack pointer
     * @return the value removed from the top of the stack
     */
    uint8_t pullStack();

    // PC Register: Address of the next instruction to be executed
    uint16_t mProgramCounter;

    // SP Register: Offset from 0x0100 (stack is between 0x0100 and 0x01FF)
    uint8_t mStackPointer;

    // S Register: Processor status flags
    uint8_t mStatusRegister;

    // A Register: Arithmetic and logic operations results
    uint8_t mAccumulator;

    // X Register: Used as counter or offset for addressing, can get/set the SP value
    uint8_t mXRegister;

    // Y Register: Used as counter or offset for addressing, can't access to the SP
    uint8_t mYRegister;

    // Addressing mode
    Addressing mAddressingMode;

    // Page boundary crossed
    bool mPageBoundaryCrossed;

    // Remaining cycles counter
    uint8_t mRemainingCycles;

    // NES
    Nes* mNes;

protected:

    typedef void (Mos6502::*OpcodeFunction)();

    /**
     * Add with carry
     */
    void ADC();

    /**
     * AND
     */
    void AND();

    /**
     * Arithmetic Shift Left
     */
    void ASL();

    /**
     * Branch on carry clear
     */
    void BCC();

    /**
     * Branch on carry set
     */
    void BCS();

    /**
     * Branch on equal
     */
    void BEQ();

    /**
     * Bit Test
     */
    void BIT();

    /**
     * Branch on minus
     */
    void BMI();

    /**
     * Branch if not equal
     */
    void BNE();

    /**
     * Branch on plus
     */
    void BPL();

    /**
     * Break
     */
    void BRK();

    /**
     * Branch on overflow clear
     */
    void BVC();

    /**
     * Branch on overflow set
     */
    void BVS();

    /**
     * Clear carry flag
     */
    void CLC();

    /**
     * Clear decimal flag
     */
    void CLD();

    /**
     * Clear interrupt flag
     */
    void CLI();

    /**
     * Clear overflow flag
     */
    void CLV();

    /**
     * Substracts value2 from value1, and updates status register bits
     * @param value1
     * @param value2
     */
    void compare(uint8_t value1, uint8_t value2);

    /**
     * Compare Accumulator
     */
    void CMP();

    /**
     * Compare X register
     */
    void CPX();

    /**
     * Compare Y register
     */
    void CPY();

    /**
     * Decrement memory
     */
    void DEC();

    /**
     * Decrement X register
     */
    void DEX();

    /**
     * Decrement Y register
     */
    void DEY();

    /**
     * Exclusive OR
     */
    void EOR();

    /**
     * Increment memory
     */
    void INC();

    /**
     * Increment X register
     */
    void INX();

    /**
     * Increment Y register
     */
    void INY();

    /**
     * Jump
     */
    void JMP();

    /**
     * Jump to Subroutine
     */
    void JSR();

    /**
     * Load accumulator
     */
    void LDA();

    /**
     * Load X register
     */
    void LDX();

    /**
     * Load Y register
     */
    void LDY();

    /**
     * Logical Shift Right
     */
    void LSR();

    /**
     * No operation
     */
    void NOP();

    /**
     * OR Accumulator
     */
    void ORA();

    /**
     * Push Accumulator
     */
    void PHA();

    /**
     * Push Status Register
     */
    void PHP();

    /**
     * Pull Accumulator
     */
    void PLA();

    /**
     * Pull Status Register
     */
    void PLP();

    /**
     * Rotate Left
     */
    void ROL();

    /**
     * Rotate Right
     */
    void ROR();

    /**
     * Return from Interrupt
     */
    void RTI();

    /**
     * Return from Subroutine
     */
    void RTS();

    /**
     * Substract with carry
     */
    void SBC();

    /**
     * Set carry flag
     */
    void SEC();

    /**
     * Set decimal flag
     */
    void SED();

    /**
     * Set interrupt flag
     */
    void SEI();

    /**
     * Store accumulator
     */
    void STA();

    /**
     * Store X register
     */
    void STX();

    /**
     * Store Y register
     */
    void STY();

    /**
     * Transfer A to X
     */
    void TAX();

    /**
     * Transfer A to Y
     */
    void TAY();

    /**
     * Transfer Stack pointer to X
     */
    void TSX();

    /**
     * Transfer X to A
     */
    void TXA();

    /**
     * Transfer X to Stack pointer
     */
    void TXS();

    /**
     * Transfer Y to A
     */
    void TYA();

    /**
     * Undefined, illegal opcode
     */
    void UND();

};

#endif // MOS6502_H
