#include "nesmemory.h"

#include "nes.h"
#include "nescartridge.h"

NesMemory::NesMemory(Nes* nes) : QObject(nes),
    mNes(nes),
    mMemory(),
    mHistory()
{
}

uint8_t NesMemory::read(uint16_t address)
{
    uint8_t value;
    if (address < INTERNAL_MEMORY_SIZE) {
        // Read from internal memory
        value = mMemory.at(unmirror(address));
    } else {
        // Read from PRG-ROM (Cartridge)
        value = mNes->cartridge()->read(address - INTERNAL_MEMORY_SIZE);
    }

    // This is only used in tests
    mHistory.push(value);
    if (mHistory.size() > 3) {
        mHistory.pop();
    }

    return value;
}

uint8_t NesMemory::readHistory()
{
    if (mHistory.size() > 0) {
        uint8_t value = mHistory.front();
        mHistory.pop();
        return value;
    }
    return 0;
}

void NesMemory::write(uint16_t address, uint8_t value)
{
    if (address >= INTERNAL_MEMORY_SIZE) {
        // The cartridge is read-only
        return;
    }

    mMemory[unmirror(address)] = value;
}

uint8_t& NesMemory::operator[](uint16_t address)
{
    return mMemory[unmirror(address)];
}

uint16_t NesMemory::unmirror(uint16_t address) const
{
    if (address < 0x2000) {
        // Values in [0x0000; 0x07FF] are mirrored 3 times in [0x0800; 0x1FFF]
        return address % 0x0800;
    }

    if (address > 0x2007 && address < 0x4000) {
        // Values in [0x2000; 0x2007] are mirrored every 8 bytes in [0x2008; 0x3FFF]
        return 0x2000 + (address % 0x8);
    }

    return address;
}
