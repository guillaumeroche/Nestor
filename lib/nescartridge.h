#ifndef NESCARTRIDGE_H
#define NESCARTRIDGE_H

#include <QObject>
#include <QByteArray>

class QFile;

class Nes;

class NesCartridge : public QObject
{
    Q_OBJECT
public:
    explicit NesCartridge(Nes* nes);

    enum class Mirroring {
        Unknown = -1,
        Horizontal = 0,
        Vertical = 1,
        FourScreen,
        SingleScreen
    };

    enum NesFileVersion {
        Archaic = 0,
        Nes1,
        Nes2
    };

public slots:

    /**
     * Loads a ROM file.
     * Emits fileLoaded signal when the file is correctly loaded. Otherwise, emits errorOccured signal
     * @param romFile the file to load
     */
    void loadFromFile(QFile& romFile);

    /**
     * Reads a value from the cartridge
     * @param address the address to read at
     * @return the value which is at the given address
     */
    uint8_t read(uint16_t address) const;

private:
    // NES
    Nes* mNes;

    // Mirroring mode
    Mirroring mMirroring;

    // Cartridge contains battery-backed PRG RAM ($6000-7FFF) or other persistent memory
    bool mHasBatteryBackedRAM;

    // Memory mapper type
    uint8_t mMapperType;

    // Number of 8 KB RAM banks
    uint8_t mRAMBankNumber;

    // Optional trainer
    QByteArray mTrainer;

    // Instructions to emulate
    QByteArray mProgramROM;

    // Graphics data
    QByteArray mCharacterROM;

signals:
    void errorOccured(const QString& message);
    void fileLoaded();

};

#endif // NESCARTRIDGE_H
