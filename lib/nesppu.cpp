#include "nesppu.h"

#include "nes.h"
#include "nesmemory.h"

static constexpr uint16_t MAX_CYCLE = 340;

NesPpu::NesPpu(Nes* nes) : QObject(nes),
    // Registers are memory-mapped
    mControlRegister1((*nes->memory())[0x2000]),
    mControlRegister2((*nes->memory())[0x2001]),
    mStatusRegister((*nes->memory())[0x2002]),
    mSpriteAddressRegister((*nes->memory())[0x2003]),
    mSpriteIORegister((*nes->memory())[0x2004]),
    mVideoAddressRegister1((*nes->memory())[0x2005]),
    mVideoAddressRegister2((*nes->memory())[0x2006]),
    mVideoIORegister((*nes->memory())[0x2007]),
    mSpriteDMARegister((*nes->memory())[0x4014])
{
}

void NesPpu::runCycle()
{
    // Increment at beginning, it's easier to handle
    mCycle++;
    if (mRemainingCycles > 0) {
        mRemainingCycles--;
        return;
    }

    if (mCycle > MAX_CYCLE) {
        // Reset
        mCycle = mCycle % MAX_CYCLE;
    }

    if (mCycle == 0) {
        return;
    }

    if (mCycle <= 256) {
        // Fetch tile data from memory
        mRemainingCycles++;
        return;
    }
}

uint8_t NesPpu::read(uint16_t address) const
{
    return mVideoMemory[unmirror(address)];
}

void NesPpu::write(uint16_t address, uint8_t value)
{
    mVideoMemory[unmirror(address)] = value;
}

uint8_t& NesPpu::controlRegister1() const
{
    return mControlRegister1;
}

uint8_t& NesPpu::controlRegister2() const
{
    return mControlRegister2;
}

uint8_t& NesPpu::statusRegister() const
{
    return mStatusRegister;
}

uint8_t& NesPpu::spriteAddressRegister() const
{
    return mSpriteAddressRegister;
}

uint8_t& NesPpu::spriteIORegister() const
{
    return mSpriteIORegister;
}

uint8_t& NesPpu::videoAddressRegister1() const
{
    return mVideoAddressRegister1;
}

uint8_t& NesPpu::videoAddressRegister2() const
{
    return mVideoAddressRegister2;
}

uint8_t& NesPpu::videoIORegister() const
{
    return mVideoIORegister;
}

uint8_t& NesPpu::spriteDMARegister() const
{
    return mSpriteDMARegister;
}

uint16_t NesPpu::unmirror(uint16_t address) const
{
    if (address >= VIDEO_MEMORY_SIZE) {
        // Logical VRAM addressing goes from 0x0 to 0xFFFF,
        // which correspond to an uint16_t address.
        // VRAM size is actually 0x4000 bytes, which is mirrored 3 times.
        return address % VIDEO_MEMORY_SIZE;
    }

    if (address >= 0x3F20) {
        // [0x3F20, 0x3FFF] mirrors [0x3F00; 0x3F1F]
        return 0x3F00 + (address % 0x1F);
    }

    if (address >= 0x3000 && address <= 0x3EFF) {
        // [0x3000, 0x3EFF] mirrors [0x2000; 0x0x2EFF]
        return address - 0x1000;
    }

    return address;
}
