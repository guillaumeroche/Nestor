QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \
    tst_mos6502.cpp

INCLUDEPATH += ../lib/

# Expose project path to test cases
DEFINES += PROJECT_PATH="\'\"$$PWD\"\'"

LIBS += -L$$OUT_PWD/../lib -lnestor
