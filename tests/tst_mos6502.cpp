#include <QtTest>

#include "nes.h"
#include "nescartridge.h"
#include "nesmemory.h"
#include "nesppu.h"
#include "mos6502.h"

class tst_mos6502 : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();
    void test_memory();
    void test_ppu_registers();

};

void tst_mos6502::initTestCase()
{
}

void tst_mos6502::cleanupTestCase()
{
}

void tst_mos6502::test_case1()
{
    QFile rom(QString(PROJECT_PATH).append("/data/nestest.nes"));
    QVERIFY(rom.exists());

    Nes nes;
    NesCartridge* nesCartridge = nes.cartridge();
    Mos6502* nesCpu = nes.cpu();

    QSignalSpy signalSpy(nesCartridge, &NesCartridge::fileLoaded);

    nes.initialize(rom);

    QCOMPARE(signalSpy.count(), 1);

    QFile log(QString(PROJECT_PATH).append("/data/nestest.log"));
    QVERIFY(log.open(QFile::ReadOnly));

    // Force program counter to 0xC000 which is the "automation" start address
    const uint16_t startAddress = 0xC000;
    nesCpu->initialize(startAddress);

    qint64 lineLength;
    // PPU cycle count, which is 3 times the CPU cycle count
    qint16 cycleCount = 0;
    while (true) {
        char buf[100];
        lineLength = log.readLine(buf, sizeof(buf));
        if (lineLength == 0) {
            break;
        }
        QString line(buf);
        //qDebug() << "Executing" << line;
        bool ok;
        uint16_t expectedPC = static_cast<uint16_t>(line.mid(0, 4).toUInt(&ok, 16));
        QVERIFY(ok);
        QCOMPARE(nesCpu->mProgramCounter, expectedPC);

        // Check registers
        uint8_t expectedValue = static_cast<uint8_t>(line.mid(50, 2).toInt(&ok, 16));
        QVERIFY(ok);
        QCOMPARE(nesCpu->mAccumulator, expectedValue);

        expectedValue = static_cast<uint8_t>(line.mid(55, 2).toInt(&ok, 16));
        QVERIFY(ok);
        QCOMPARE(nesCpu->mXRegister, expectedValue);

        expectedValue = static_cast<uint8_t>(line.mid(60, 2).toInt(&ok, 16));
        QVERIFY(ok);
        QCOMPARE(nesCpu->mYRegister, expectedValue);

        uint8_t expectedP = static_cast<uint8_t>(line.mid(65, 2).toUInt(&ok, 16));
        QVERIFY(ok);
        QCOMPARE(nesCpu->mStatusRegister, expectedP);

        uint8_t expectedSP = static_cast<uint8_t>(line.mid(71, 2).toUInt(&ok, 16));
        QVERIFY(ok);
        QCOMPARE(nesCpu->mStackPointer, expectedSP);

        qint16 expectedCycleCount = static_cast<qint16>(line.mid(78, 3).toUInt(&ok));
        QVERIFY(ok);
        QCOMPARE(cycleCount, expectedCycleCount);

        nesCpu->runCycle();
        cycleCount += 3;

        while (nesCpu->mRemainingCycles > 0) {
            nesCpu->runCycle();
            cycleCount += 3;
        }

        if (cycleCount > 340) {
            cycleCount -= 341;
        }

        if (expectedPC == 0xC6BD) {
            // Don't test illegal opcodes for now
            break;
        }
    }

}

void tst_mos6502::test_memory()
{
    NesMemory memory(nullptr);
    uint16_t address = 0x666;
    uint8_t value = 0xAA;
    memory.write(address, value);

    // Test read
    QCOMPARE(memory.read(address), value);

    // Test mirroring [0x0000; 0x07FF]
    QCOMPARE(memory.read(address + 0x0800), value);
    QCOMPARE(memory.read(address + (0x0800 * 2)), value);
    QCOMPARE(memory.read(address + (0x0800 * 3)), value);

    // Test mirroring [0x2000; 0x2007]
    for (uint8_t i = 0; i < 8; i++) {
        memory.write(0x2000 + i, i);
    }
    for (uint16_t i = 0x2008; i < 0x4000; i++) {
        QCOMPARE(uint16_t(memory.read(i)), uint16_t(i % 8));
    }

    // Test [] operator
    value = 0xBB;
    memory[address] = value;
    QCOMPARE(memory.read(address), value);
    QCOMPARE(memory[address], value);
}

/**
 * Tests PPU registers memory mapping
 */
void tst_mos6502::test_ppu_registers()
{
    Nes nes;
    NesMemory* memory = nes.memory();
    QVERIFY(memory);
    NesPpu* ppu = nes.ppu();
    QVERIFY(ppu);

    QCOMPARE(ppu->controlRegister1(), uint8_t(0));
    QCOMPARE(ppu->controlRegister2(), uint8_t(0));
    QCOMPARE(ppu->statusRegister(), uint8_t(0));
    QCOMPARE(ppu->spriteAddressRegister(), uint8_t(0));
    QCOMPARE(ppu->spriteIORegister(), uint8_t(0));
    QCOMPARE(ppu->videoAddressRegister1(), uint8_t(0));
    QCOMPARE(ppu->videoAddressRegister2(), uint8_t(0));
    QCOMPARE(ppu->videoIORegister(), uint8_t(0));
    QCOMPARE(ppu->spriteDMARegister(), uint8_t(0));

    // Write some values to memory
    for (uint8_t i = 0; i < 8; i++) {
        memory->write(0x2000 + i, i + 1);
    }

    // Check that registers have the correct values
    QCOMPARE(ppu->controlRegister1(), uint8_t(1));
    QCOMPARE(ppu->controlRegister2(), uint8_t(2));
    QCOMPARE(ppu->statusRegister(), uint8_t(3));
    QCOMPARE(ppu->spriteAddressRegister(), uint8_t(4));
    QCOMPARE(ppu->spriteIORegister(), uint8_t(5));
    QCOMPARE(ppu->videoAddressRegister1(), uint8_t(6));
    QCOMPARE(ppu->videoAddressRegister2(), uint8_t(7));
    QCOMPARE(ppu->videoIORegister(), uint8_t(8));

    memory->write(0x4014, 9);
    QCOMPARE(ppu->spriteDMARegister(), uint8_t(9));
}


QTEST_APPLESS_MAIN(tst_mos6502)
#include "tst_mos6502.moc"
