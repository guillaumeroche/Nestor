TEMPLATE = subdirs

SUBDIRS += app
SUBDIRS += lib
SUBDIRS += tests

app.depends = lib
tests.depends = lib
