# Requirements

* Qt 5.8.0 or higher
* Linux

Untested on Windows and Mac platforms for now, it may or may not work.

# Building

Open the project in Qt Creator and hit Ctrl+B.

Or, in a terminal emulator, run:
```
qmake
make
```

# Running

For now, Nestor is a command line executable. You must provide the path of a .nes ROM file as argument.

# Special thanks

I would like to thank my employer, [Genymobile](https://www.genymobile.com), which has sponsored some parts of this project for Hacktoberfest 2020. Genymobile is the editor of [Genymotion](https://www.genymotion.com), a powerfull Android emulator available for desktop and in the cloud.

